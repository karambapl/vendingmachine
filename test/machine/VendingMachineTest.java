/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package machine;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import items.Item;
import money.Cashier;
import money.Coin;
import money.Wallet;

/**
 * Unit tests for {@link VendingMachine}
 */
public class VendingMachineTest {
	
	private static final float ONE_POUND = 1.0f;
	private static final float FIFTY_PENCE = 0.5f;
	private static final float TWENTY_PENCE = 0.2f;
	private static final float TEN_PENCE = 0.1f;
	
	private static Coin coinTenPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, TEN_PENCE);
	private static Coin coinTwentyPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, TWENTY_PENCE);
	private static Coin coinFiftyPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, FIFTY_PENCE);
	private static Coin coinOnePound = new Coin(Wallet.DEFAULT_CURRENCY_UK, ONE_POUND);
	
	private static Item itemA = new Item("A", 0.6f);
	private static Item itemB = new Item("B", 1.0f);
	private static Item itemC = new Item("C", 1.7f);
	
	private static Wallet wallet = null;	

	private static Cashier cashier = null;

	private static ItemsStorage itemsStorage = null;
	
	private static VendingMachine vendingMachine = null;
		
	@Before
	public void initVendingMachine() {

		wallet = new Wallet(Wallet.DEFAULT_CURRENCY_UK);	

		cashier = new Cashier(wallet);

		itemsStorage = new ItemsStorage();
		
		vendingMachine = new VendingMachine(cashier, itemsStorage);
		
	}
	
	@Test
	public void defaultStateIsOff() {
		assertFalse(vendingMachine.isOn());
	}
	
	@Test
	public void turnsOn() {
		vendingMachine.setOn();
		
		assertTrue(vendingMachine.isOn());		
	}
	
	@Test
	public void turnsOff() {
		vendingMachine.setOff();
		
		assertFalse(vendingMachine.isOn());		
	}
	
	@Test
	public void turnsOffSelectItem() { 
		vendingMachine.setOff();
		
		assertFalse(vendingMachine.selectItem(itemA));		
	}
	
	@Test
	public void turnsOnSelectItem() {
		vendingMachine.setOn();
		
		assertTrue(vendingMachine.selectItem(itemA));		
	}
	
	@Test
	public void defaultStateIsOffInsertCoin() {
		VendingMachine vendingMachine = new VendingMachine(newCashier(), new ItemsStorage());
		assertFalse( vendingMachine.insertCoin(coinOnePound) );
	
	}
	
	@Test 
	public void turnsOnInsertCoinGetMoneyBack() {
		VendingMachine vendingMachine = new VendingMachine(newCashier(), new ItemsStorage());
		vendingMachine.setOn();
		
		assertTrue( vendingMachine.insertCoin(coinTenPence) );
		assertTrue( vendingMachine.insertCoin(coinTwentyPence) );
		assertTrue( vendingMachine.insertCoin(coinFiftyPence) );
		assertTrue( vendingMachine.insertCoin(coinOnePound) );
		
		List<Coin> getMoneyBack = vendingMachine.getMoneyBack();
		
		Float getMoneyBackSummation = getMoneyBack
				.stream()
				.map(Coin::getValue)
				.reduce(0.f, (a, b) -> a + b);
		
		assertEquals(1.8f, getMoneyBackSummation.floatValue(), Item.EPSILON);
		assertEquals(4, getMoneyBack.size() );
	} 
	
	private Cashier newCashier() {
		Wallet wallet = new Wallet(Wallet.DEFAULT_CURRENCY_UK);			
		wallet.addToPossible(coinTenPence);			
		wallet.addToPossible(coinTwentyPence);			
		wallet.addToPossible(coinFiftyPence);			
		wallet.addToPossible(coinOnePound);
				
		Cashier cashier = new Cashier(wallet);
	
		return cashier;
	}
	 
}
