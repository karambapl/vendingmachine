/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package machine;

import static org.junit.Assert.*;

import org.junit.Test;

public class UtilCheckTest {

	 
	@Test
	public void testIsIntegerValidNotZero() {
		assertFalse( UtilCheck.isIntegerValidNotZero(null) );
		assertFalse( UtilCheck.isIntegerValidNotZero(new Integer(0)) );
		assertTrue( UtilCheck.isIntegerValidNotZero(new Integer(1)) );
	}

}
