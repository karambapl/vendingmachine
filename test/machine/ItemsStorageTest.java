/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package machine;

import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;

import items.Item;

/**
 * Unit tests for {@link ItemsStorage}
 */
public class ItemsStorageTest {
	private static final float EPSILON = 0.00001f;
	
	private static ItemsStorageObserver itemsStorageObserver1 = new ItemsStorageObserver() {
		
		@Override
		public void onNoItemInStorage(String itemName) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onItemAvailable(Item item) {
			// TODO Auto-generated method stub
			
		}
	};
	
	private static ItemsStorageObserver itemsStorageObserver2 = new ItemsStorageObserver() {
		
		@Override
		public void onNoItemInStorage(String itemName) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onItemAvailable(Item item) {
			// TODO Auto-generated method stub
			
		}
	};
	
	private static Item itemA = new Item("A", 0.6f);
	private static Item itemB = new Item("B", 1.0f);
	private static Item itemC = new Item("C", 1.7f);
	

	@Test
	public void testItemsStorage() {
		ItemsStorage itemsStorage = new ItemsStorage();
		
		assertNotNull(itemsStorage.getPossibleItems());
		assertNotNull(itemsStorage.getStorageItemMap());
	}

	@Test
	public void testAddObserver() {
		ItemsStorage itemsStorage = new ItemsStorage();
		
		assertTrue( itemsStorage.addObserver(itemsStorageObserver1) );
		assertTrue( itemsStorage.addObserver(itemsStorageObserver2) );
		assertFalse( itemsStorage.addObserver(itemsStorageObserver1) );
	}

	@Test
	public void testRemoveObserver() {
		ItemsStorage itemsStorage = new ItemsStorage();
		
		itemsStorage.addObserver(itemsStorageObserver1);
		itemsStorage.addObserver(itemsStorageObserver2);
		
		assertTrue( itemsStorage.removeObserver(itemsStorageObserver1) );
		assertFalse( itemsStorage.removeObserver(itemsStorageObserver1) );
		
		assertTrue( itemsStorage.removeObserver(itemsStorageObserver2) );
		assertFalse( itemsStorage.removeObserver(itemsStorageObserver2) );
	}

	@Test
	public void testAddItem() {
		ItemsStorage itemsStorage = new ItemsStorage();
		
		itemsStorage.addItem(itemA);
		
		for(int i=0; i<2; ++i) {
			itemsStorage.addItem(itemB);
		}
		
		for(int i=0; i<3; ++i) {
			itemsStorage.addItem(itemC);
		}
		
		Map<Integer, Item> possibleItems = itemsStorage.getPossibleItems();
		Map<Integer, Integer> storageItemMap = itemsStorage.getStorageItemMap();
		
		assertEquals( possibleItems.get( itemA.getName().hashCode() ).getPrice(), itemA.getPrice(), EPSILON );
		assertEquals( possibleItems.get( itemB.getName().hashCode() ).getPrice(), itemB.getPrice(), EPSILON );
		assertEquals( possibleItems.get( itemC.getName().hashCode() ).getPrice(), itemC.getPrice(), EPSILON );
		
		assertEquals(1, storageItemMap.get(itemA.getName().hashCode()).intValue() );
		assertEquals(2, storageItemMap.get(itemB.getName().hashCode()).intValue() );
		assertEquals(3, storageItemMap.get(itemC.getName().hashCode()).intValue() );		
	}

	@Test
	public void testIsAvailable() {
		ItemsStorage itemsStorage = new ItemsStorage();
		
		itemsStorage.addItem(itemA);
		
		for(int i=0; i<2; ++i) {
			itemsStorage.addItem(itemB);
		}

		
		assertTrue(itemsStorage.isAvailable(itemA.getName()) );
		assertTrue(itemsStorage.isAvailable(itemB.getName()) );
		assertFalse(itemsStorage.isAvailable(itemC.getName()) );		
		
	}

	static boolean onNoItemInStorageCalled;
	static String onNoItemInStorageCalledItemName;
	static boolean onItemAvailableCalled;
	static Item onItemAvailableCalledItem;
	
	@Test
	public void testGetItem_noItem() {
		ItemsStorage itemsStorage = new ItemsStorage();
		
		onNoItemInStorageCalled = false;
		onItemAvailableCalled = false;
		onNoItemInStorageCalledItemName = null;
		onItemAvailableCalledItem = null;
		
		itemsStorage.addObserver(new ItemsStorageObserver() {
			
			@Override
			public void onNoItemInStorage(String itemName) {
				onNoItemInStorageCalled = true;
				onNoItemInStorageCalledItemName = itemName;
			}
			
			@Override
			public void onItemAvailable(Item item) {
				onItemAvailableCalled = true;
				onItemAvailableCalledItem = item;
			}
		});
		
		
		{
			
			onNoItemInStorageCalled = false;
			onItemAvailableCalled = false;
			onNoItemInStorageCalledItemName = null;
			onItemAvailableCalledItem = null;
			
			Item returnedItem1 = itemsStorage.getItem(itemA.getName());		
			assertNull(returnedItem1);
			assertFalse(onItemAvailableCalled);
			assertTrue(onNoItemInStorageCalled);
			assertEquals(itemA.getName(), onNoItemInStorageCalledItemName);
		}
		
		
	
		
	}	
	
	@Test
	public void testGetItem_bigTest() {
		ItemsStorage itemsStorage = new ItemsStorage();
		
		onNoItemInStorageCalled = false;
		onItemAvailableCalled = false;
		onNoItemInStorageCalledItemName = null;
		onItemAvailableCalledItem = null;
		
		itemsStorage.addObserver(new ItemsStorageObserver() {
			
			@Override
			public void onNoItemInStorage(String itemName) {
				onNoItemInStorageCalled = true;
				onNoItemInStorageCalledItemName = itemName;
			}
			
			@Override
			public void onItemAvailable(Item item) {
				onItemAvailableCalled = true;
				onItemAvailableCalledItem = item;
			}
		});
		
		itemsStorage.addItem(itemA);
		
		for(int i=0; i<2; ++i) {
			itemsStorage.addItem(itemB);
		}
		
		for(int i=0; i<3; ++i) {
			itemsStorage.addItem(itemC);
		}
		
		
		{
			Item returnedItem = itemsStorage.getItem(itemA.getName());		
			assertEquals(itemA.getName(), returnedItem.getName());
			assertTrue(onItemAvailableCalled);
			assertEquals(itemA.getName(), onItemAvailableCalledItem.getName());		
			assertFalse(onNoItemInStorageCalled);		
			
			onNoItemInStorageCalled = false;
			onItemAvailableCalled = false;
			onNoItemInStorageCalledItemName = null;
			onItemAvailableCalledItem = null;
			
			Item returnedItem2 = itemsStorage.getItem(itemA.getName());		
			assertNull(returnedItem2);
			assertFalse(onItemAvailableCalled);
			assertTrue(onNoItemInStorageCalled);
			assertEquals(itemA.getName(), onNoItemInStorageCalledItemName);
		}
		
		
		{			
		
			for(int i=0; i<3; ++i) {
				onNoItemInStorageCalled = false;
				onItemAvailableCalled = false; 
				onNoItemInStorageCalledItemName = null;
				onItemAvailableCalledItem = null;
				
				Item returnedItem = itemsStorage.getItem(itemC.getName());		
				assertEquals(itemC.getName(), returnedItem.getName());
				assertTrue(onItemAvailableCalled);
				assertEquals(itemC.getName(), onItemAvailableCalledItem.getName());		
				assertFalse(onNoItemInStorageCalled);		
			}
			
			onNoItemInStorageCalled = false;
			onItemAvailableCalled = false;
			onNoItemInStorageCalledItemName = null;
			onItemAvailableCalledItem = null;
			
			Item returnedItem2 = itemsStorage.getItem(itemC.getName());		
			assertNull(returnedItem2);
			assertFalse(onItemAvailableCalled);
			assertTrue(onNoItemInStorageCalled);
			assertEquals(itemC.getName(), onNoItemInStorageCalledItemName);
		
		}
		
	}

}
