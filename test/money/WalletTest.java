/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package money;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import money.Coin;
import money.Wallet;

/**
 * Unit tests for {@link Wallet}
 */
public class WalletTest {
	
	private static final float EPSILON = 0.00001f;
	private static final String INVALID_CURRENCY = "InvalidCurrency";
	private static final float ONE_POUND = 1.0f;
	private static final float FIFTY_PENCE = 0.5f;
	private static final float TWENTY_PENCE = 0.2f;
	private static final float TEN_PENCE = 0.1f;
	
	private static Coin coinTenPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, TEN_PENCE);
	private static Coin coinTwentyPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, TWENTY_PENCE);
	private static Coin coinFiftyPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, FIFTY_PENCE);
	private static Coin coinOnePound = new Coin(Wallet.DEFAULT_CURRENCY_UK, ONE_POUND);

	
	@Before
	public void initWallet() {
		
	
	}
	
	@Test
	public void currencyTest() { 
		String currency = Wallet.DEFAULT_CURRENCY_UK;
		Wallet wallet = new Wallet(currency);	
		
		assertEquals( currency, wallet.getCurrency() );
		
		assertTrue( wallet.isCurrencyValid(currency) );		
		
		assertFalse( wallet.isCurrencyValid(INVALID_CURRENCY) );
		
	}
	
	@Test
	public void currencyAddToPossibleTest() { 
		String currency = Wallet.DEFAULT_CURRENCY_UK;
		Wallet wallet = new Wallet(currency);	
		
		Coin invalidCurrencyCoin = new Coin(INVALID_CURRENCY, TEN_PENCE);
		
		assertFalse( wallet.addToPossible( invalidCurrencyCoin ) );
		
		assertTrue( wallet.addToPossible( coinFiftyPence ) );
		assertFalse( wallet.addToPossible( coinFiftyPence ) );
		
		assertTrue( wallet.addToPossible( coinOnePound ) );		
		
		List<Float> possibleCoins = wallet.getPossibleCoins();
		
		assertEquals(2, possibleCoins.size());
		
		assertTrue( possibleCoins.contains(coinFiftyPence.getValue()) );
		assertTrue( possibleCoins.contains(coinOnePound.getValue()) );
		
	}
	
	@Test
	public void isCurrencyValidTest() { 
		String currency = Wallet.DEFAULT_CURRENCY_UK;
		Wallet wallet = new Wallet(currency);	
		
		Coin invalidCurrencyCoin = new Coin(INVALID_CURRENCY, TEN_PENCE);
		
		assertFalse( wallet.isCurrencyValid( invalidCurrencyCoin.getCurrency() ) );
		assertTrue( wallet.isCurrencyValid( coinOnePound.getCurrency() ) );		
	}
	
	@Test
	public void isValueValidCheckValidTest() { 
		String currency = Wallet.DEFAULT_CURRENCY_UK;
		Wallet wallet = new Wallet(currency);	
		wallet.addToPossible(coinOnePound);
		wallet.addToPossible(coinFiftyPence);
		wallet.addToPossible(coinTenPence);
		wallet.addToPossible(coinTwentyPence);
	
		Coin validValueCoin1 = new Coin(INVALID_CURRENCY, 1.0000f);
		assertTrue( wallet.isValueValid(validValueCoin1.getValue()));
		
		Coin validValueCoin2 = new Coin(INVALID_CURRENCY, 0.5000f);
		assertTrue( wallet.isValueValid(validValueCoin2.getValue()));
		
		Coin validValueCoin3 = new Coin(INVALID_CURRENCY, 0.1000f);
		assertTrue( wallet.isValueValid(validValueCoin3.getValue()));
		
		Coin validValueCoin4 = new Coin(INVALID_CURRENCY, 0.2000f);
		assertTrue( wallet.isValueValid(validValueCoin4.getValue()));	
	}
	
	@Test
	public void isValueValidCheckInvalidTest() { 
		String currency = Wallet.DEFAULT_CURRENCY_UK;
		Wallet wallet = new Wallet(currency);	
		wallet.addToPossible(coinOnePound);
		wallet.addToPossible(coinFiftyPence);
		wallet.addToPossible(coinTenPence);
		wallet.addToPossible(coinTwentyPence);
		
		Coin invalidValueCoin1 = new Coin(INVALID_CURRENCY, 1.0001f);
		Coin invalidValueCoin2 = new Coin(INVALID_CURRENCY, 0.9999f);
		assertFalse( wallet.isValueValid( invalidValueCoin1.getValue() ) );
		assertFalse( wallet.isValueValid( invalidValueCoin2.getValue() ) );
		
		Coin invalidValueCoin3 = new Coin(INVALID_CURRENCY, 0.5001f);
		Coin invalidValueCoin4 = new Coin(INVALID_CURRENCY, 0.4999f);
		assertFalse( wallet.isValueValid( invalidValueCoin3.getValue() ) );
		assertFalse( wallet.isValueValid( invalidValueCoin4.getValue() ) );
		
		Coin invalidValueCoin5 = new Coin(INVALID_CURRENCY, 0.1001f);
		Coin invalidValueCoin6 = new Coin(INVALID_CURRENCY, 0.9999f);
		assertFalse( wallet.isValueValid( invalidValueCoin5.getValue() ) );
		assertFalse( wallet.isValueValid( invalidValueCoin6.getValue() ) );
		
		Coin invalidValueCoin7 = new Coin(INVALID_CURRENCY, 0.2001f);
		Coin invalidValueCoin8 = new Coin(INVALID_CURRENCY, 0.1999f);
		assertFalse( wallet.isValueValid( invalidValueCoin7.getValue() ) );
		assertFalse( wallet.isValueValid( invalidValueCoin8.getValue() ) );
	
	
	}
	
	
	@Test
	public void addCoinTest() { 
		Wallet wallet = new Wallet("");	

	
		wallet.addCoin(TEN_PENCE);
		
		wallet.addCoin(TWENTY_PENCE);	
		wallet.addCoin(TWENTY_PENCE);
		
		wallet.addCoin(FIFTY_PENCE);
		wallet.addCoin(FIFTY_PENCE);
		wallet.addCoin(FIFTY_PENCE);
		
		wallet.addCoin(ONE_POUND);
		wallet.addCoin(ONE_POUND);
		wallet.addCoin(ONE_POUND);
		wallet.addCoin(ONE_POUND);
		
		
		Map<Float, Integer> coinsValueMap = wallet.getWalletsCoinsMap();
		
		assertEquals( 1, coinsValueMap.get(TEN_PENCE).intValue() );
		assertEquals( 2, coinsValueMap.get(TWENTY_PENCE).intValue() );
		assertEquals( 3, coinsValueMap.get(FIFTY_PENCE).intValue() );
		assertEquals( 4, coinsValueMap.get(ONE_POUND).intValue());
	}
	
	
	@Test
	public void getCoinTest() { 
		Wallet wallet = new Wallet("");	

	
		wallet.addCoin(TEN_PENCE);
		
		wallet.addCoin(TWENTY_PENCE);	
		wallet.addCoin(TWENTY_PENCE);
		
		wallet.addCoin(FIFTY_PENCE);
		wallet.addCoin(FIFTY_PENCE);
		wallet.addCoin(FIFTY_PENCE);
		
		wallet.addCoin(ONE_POUND);
		wallet.addCoin(ONE_POUND);
		wallet.addCoin(ONE_POUND);
		wallet.addCoin(ONE_POUND);
	
		
		assertEquals( wallet.getCoin(TEN_PENCE), TEN_PENCE, EPSILON );
		assertNotEquals( wallet.getCoin(TEN_PENCE), TEN_PENCE, EPSILON ); //There isn't coin - it was returned 

		assertEquals( wallet.getCoin(TWENTY_PENCE), TWENTY_PENCE, EPSILON  );
		assertEquals( wallet.getCoin(TWENTY_PENCE), TWENTY_PENCE, EPSILON  );
		assertNotEquals( wallet.getCoin(TWENTY_PENCE), TWENTY_PENCE, EPSILON ); //There isn't coin - it was returned 
				
		assertEquals( wallet.getCoin(FIFTY_PENCE), FIFTY_PENCE, EPSILON  );
		assertEquals( wallet.getCoin(FIFTY_PENCE), FIFTY_PENCE, EPSILON  );
		assertEquals( wallet.getCoin(FIFTY_PENCE), FIFTY_PENCE, EPSILON  );
		assertNotEquals( wallet.getCoin(FIFTY_PENCE), FIFTY_PENCE, EPSILON ); //There isn't coin - it was returned 
		
		assertEquals( wallet.getCoin(ONE_POUND), ONE_POUND, EPSILON  );
		assertEquals( wallet.getCoin(ONE_POUND), ONE_POUND, EPSILON  );
		assertEquals( wallet.getCoin(ONE_POUND), ONE_POUND, EPSILON  );
		assertEquals( wallet.getCoin(ONE_POUND), ONE_POUND, EPSILON  );
		assertNotEquals( wallet.getCoin(ONE_POUND), ONE_POUND, EPSILON ); //There isn't coin - it was returned

	}
	
	@Test
	public void getAllTest() { 
		Wallet wallet = new Wallet("");	

		List<Float> coinsToGet = new ArrayList<>();
	
		wallet.addCoin(TEN_PENCE);
		coinsToGet.add(TEN_PENCE);
		
		
		for(int i=0; i<2; ++i) {
			wallet.addCoin(TWENTY_PENCE);
		}
		coinsToGet.add(TWENTY_PENCE);
		
		
		for(int i=0; i<3; ++i) {
			wallet.addCoin(FIFTY_PENCE);
		}
		coinsToGet.add(FIFTY_PENCE);
		
		
		for(int i=0; i<4; ++i) {
			wallet.addCoin(ONE_POUND);						
		}
		coinsToGet.add(ONE_POUND);
		
		
		Map<Float, Integer> coinsValueMapBefore = wallet.getWalletsCoinsMap();
		
		int n10penceBefore = coinsValueMapBefore.get(TEN_PENCE).intValue();
		int n20penceBefore = coinsValueMapBefore.get(TWENTY_PENCE).intValue();
		int n50penceBefore = coinsValueMapBefore.get(FIFTY_PENCE).intValue();
		int n1poundBefore = coinsValueMapBefore.get(ONE_POUND).intValue();
		
		
		
		List<Float> coinsGet = wallet.getAll(coinsToGet);
		
		Map<Float, Integer> coinsValueMapAfter = wallet.getWalletsCoinsMap();

		assertEquals(4, coinsGet.size() );
		assertTrue( coinsGet.contains(TEN_PENCE) );
		assertTrue( coinsGet.contains(TWENTY_PENCE) );
		assertTrue( coinsGet.contains(FIFTY_PENCE) );
		assertTrue( coinsGet.contains(ONE_POUND) );
		
		assertEquals(n10penceBefore - 1, coinsValueMapAfter.get(TEN_PENCE).intValue() );
		assertEquals(n20penceBefore - 1, coinsValueMapAfter.get(TWENTY_PENCE).intValue() );
		assertEquals(n50penceBefore - 1, coinsValueMapAfter.get(FIFTY_PENCE).intValue() );
		assertEquals(n1poundBefore - 1, coinsValueMapAfter.get(ONE_POUND).intValue() );

	}
	
	
	@Test
	public void addAllTest() { 
		Wallet wallet = new Wallet("");	

		List<Float> coinsToAdd = new ArrayList<>();
	
		coinsToAdd.add(TEN_PENCE);
		
		for(int i=0; i<2; ++i) {
			coinsToAdd.add(TWENTY_PENCE);
		}
		
		
		for(int i=0; i<3; ++i) {
			coinsToAdd.add(FIFTY_PENCE);
		}

		
		for(int i=0; i<4; ++i) {
			coinsToAdd.add(ONE_POUND);						
		}
		
		wallet.addAll(coinsToAdd);
		
		Map<Float, Integer> coinsValueMap = wallet.getWalletsCoinsMap();
		
		assertEquals( 1, coinsValueMap.get(TEN_PENCE).intValue() );
		assertEquals( 2, coinsValueMap.get(TWENTY_PENCE).intValue() );
		assertEquals( 3, coinsValueMap.get(FIFTY_PENCE).intValue() );
		assertEquals( 4, coinsValueMap.get(ONE_POUND).intValue());

	}

}
