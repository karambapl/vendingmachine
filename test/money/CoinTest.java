/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package money;

import static org.junit.Assert.*;

import org.junit.Test;

import money.Coin;
import money.Wallet;

/**
 * Unit tests for {@link Coin}
 */
public class CoinTest {
	private static final float ONE_POUND = 1.0f;
	private static final float EPSILON = 0.00001f;
	
	@Test
	public void getTest() {
		Coin coin = new Coin(Wallet.DEFAULT_CURRENCY_UK, ONE_POUND);
		
		assertEquals(Wallet.DEFAULT_CURRENCY_UK, coin.getCurrency());
		assertEquals(ONE_POUND, coin.getValue().floatValue(), EPSILON);
	}
	
	@Test
	public void copyTest() {
		Coin coin = new Coin(Wallet.DEFAULT_CURRENCY_UK, ONE_POUND);
		
		Coin newCoin = new Coin(coin);
		
		assertEquals(coin.getCurrency(), newCoin.getCurrency());
		assertEquals(coin.getValue(), newCoin.getValue(), EPSILON);
	}

}
