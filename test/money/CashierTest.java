/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package money;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import items.Item;

public class CashierTest {
	private static final String INVALID_CURRENCY = "InvalidCurrency";
	private static final float ONE_POUND = 1.0f;
	private static final float FIFTY_PENCE = 0.5f;
	private static final float TWENTY_PENCE = 0.2f;
	private static final float TEN_PENCE = 0.1f;
	
	private static Coin coinTenPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, TEN_PENCE);
	private static Coin coinTwentyPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, TWENTY_PENCE);
	private static Coin coinFiftyPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, FIFTY_PENCE);
	private static Coin coinOnePound = new Coin(Wallet.DEFAULT_CURRENCY_UK, ONE_POUND);
	
	private static TransactionObserver transactionObserverBlank = new TransactionObserver() {
		
		@Override
		public void onTransactionCompleted(Item item, List<Coin> changeCoins) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onNoEnoughtMoney(Item item, float remainedPayment) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onNoEnoughtMachineCoins() {
			// TODO Auto-generated method stub
			
		}
	};

	
	private static CoinValidatorObserver coinValidatorObserverBlank = new CoinValidatorObserver() {
		
		@Override
		public void onCoinValueInvalid(Coin coin, List<Float> validCoinsValues) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onCoinCurrencyInvalid(Coin coin, String validCurrency) {
			// TODO Auto-generated method stub
			
		}
	};
	
	private static boolean calledOnCoinValueInvalid = false;
	private static Coin calledOnCoinValueInvalidCoin = null;
	private static List<Float> calledOnCoinValueInvalidValidCoinsValues = null;
	
	private static boolean calledOnCoinCurrencyInvalid = false;
	private static Coin calledOnCoinCurrencyInvalidCoin = null;
	private static String calledOnCoinCurrencyInvalidValidCurrency = null;
	
	private static CoinValidatorObserver coinValidatorObserver = new CoinValidatorObserver() {
		
		@Override
		public void onCoinValueInvalid(Coin coin, List<Float> validCoinsValues) {
			System.out.println("\nonCoinValueInvalid");
			calledOnCoinValueInvalid = true;
			calledOnCoinValueInvalidCoin = coin;
			calledOnCoinValueInvalidValidCoinsValues = validCoinsValues;
			
			System.out.println("Valid values are : ");
			for(Float coinValue : validCoinsValues) {
				System.out.println("\t\t" + coinValue);
			}
			
			System.out.println("Returned user coin : ");
			System.out.println("\tCurrency : " + coin.getCurrency());
			System.out.println("\tValue : " + coin.getValue());
		}
		
		@Override
		public void onCoinCurrencyInvalid(Coin coin, String validCurrency) {
			System.out.println("\nonCoinCurrencyInvalid");
			calledOnCoinCurrencyInvalid = true;
			calledOnCoinCurrencyInvalidCoin = coin;
			calledOnCoinCurrencyInvalidValidCurrency = validCurrency;		
			
			System.out.println("ValidCurrency is : " + validCurrency);
			System.out.println("Returned user coin : ");
			System.out.println("\tCurrency : " + coin.getCurrency());
			System.out.println("\tValue : " + coin.getValue());
		}
	};
	private void resetCoinValidatorObserverStates() {
		calledOnCoinValueInvalid = false;
		calledOnCoinValueInvalidCoin = null;
		calledOnCoinValueInvalidValidCoinsValues = null;
		
		calledOnCoinCurrencyInvalid = false;
		calledOnCoinCurrencyInvalidCoin = null;
		calledOnCoinCurrencyInvalidValidCurrency = null;
	}
 
	@Test
	public void testCashier() {
		Cashier cashier = new Cashier(new Wallet(""));
		assertNotNull(cashier.getWallet());
	}

	@Test
	public void testAddObserverCoinValidatorObserver() {
		Cashier cashier = new Cashier(new Wallet(""));
		
		assertTrue( cashier.addObserver(coinValidatorObserverBlank) );
		assertFalse( cashier.addObserver(coinValidatorObserverBlank) );

	}

	@Test
	public void testRemoveObserverCoinValidatorObserver() {
		Cashier cashier = new Cashier(new Wallet(""));
		
		cashier.addObserver(coinValidatorObserverBlank);
		
		assertTrue( cashier.removeObserver(coinValidatorObserverBlank) );
		assertFalse( cashier.removeObserver(coinValidatorObserverBlank) );
	}

	@Test
	public void testAddObserverTransactionObserver() {
		Cashier cashier = new Cashier(new Wallet(""));
		
		assertTrue( cashier.addObserver(transactionObserverBlank) );
		assertFalse( cashier.addObserver(transactionObserverBlank) );
	}

	@Test
	public void testRemoveObserverTransactionObserver() {
		Cashier cashier = new Cashier(new Wallet(""));
		
		cashier.addObserver(transactionObserverBlank);
		
		assertTrue( cashier.removeObserver(transactionObserverBlank) );
		assertFalse( cashier.removeObserver(transactionObserverBlank) );
	}

	@Test
	public void testInsert() {
		Wallet wallet = new Wallet(Wallet.DEFAULT_CURRENCY_UK);			
		wallet.addToPossible(coinTenPence);			
		wallet.addToPossible(coinTwentyPence);			
		wallet.addToPossible(coinFiftyPence);			

		
		Cashier cashier = new Cashier(wallet);
		cashier.addObserver(coinValidatorObserver);
		//==========================================
		assertTrue(cashier.insert(new Coin(Wallet.DEFAULT_CURRENCY_UK, TEN_PENCE)) );
		assertTrue(cashier.insert(new Coin(Wallet.DEFAULT_CURRENCY_UK, TWENTY_PENCE)) );
		assertTrue(cashier.insert(new Coin(Wallet.DEFAULT_CURRENCY_UK, FIFTY_PENCE)) );
		//==========================================
		resetCoinValidatorObserverStates();
		assertFalse(cashier.insert(new Coin(Wallet.DEFAULT_CURRENCY_UK, ONE_POUND)) );
		
			//Callback:
			assertTrue(calledOnCoinValueInvalid);
			assertEquals(ONE_POUND, calledOnCoinValueInvalidCoin.getValue(), Item.EPSILON);
			assertTrue(calledOnCoinValueInvalidValidCoinsValues.contains(TEN_PENCE));
			assertTrue(calledOnCoinValueInvalidValidCoinsValues.contains(TWENTY_PENCE));
			assertTrue(calledOnCoinValueInvalidValidCoinsValues.contains(FIFTY_PENCE));
			assertFalse(calledOnCoinValueInvalidValidCoinsValues.contains(ONE_POUND));
		//==========================================
		 
		resetCoinValidatorObserverStates();
		assertFalse(cashier.insert(new Coin("PL", FIFTY_PENCE)) );	
		
			//Callback:
			assertTrue(calledOnCoinCurrencyInvalid);
			assertEquals("PL", calledOnCoinCurrencyInvalidCoin.getCurrency());
			assertEquals(Wallet.DEFAULT_CURRENCY_UK, calledOnCoinCurrencyInvalidValidCurrency );
		
		//==========================================
		
	}

	@Test 
	public void testGetInsertedCoinsBack() {
		
		Wallet wallet = new Wallet(Wallet.DEFAULT_CURRENCY_UK);			
		wallet.addToPossible(coinTenPence);			
		wallet.addToPossible(coinTwentyPence);			
		wallet.addToPossible(coinFiftyPence);			
		wallet.addToPossible(coinOnePound);
				
		Cashier cashier = new Cashier(wallet);
		
		cashier.insert(coinTenPence);
		
		for(int i=0; i<2; ++i) {
			cashier.insert(coinTwentyPence);
		}
		
		for(int i=0; i<3; ++i) {
			cashier.insert(coinFiftyPence);
		}
		
		for(int i=0; i<4; ++i) {
			cashier.insert(coinOnePound);
		}
		
		List<Coin> insertedCoinBack = cashier.getInsertedCoinsBack();
		assertEquals(10, insertedCoinBack.size());
		
		assertEquals(coinTenPence.getValue(), insertedCoinBack.get(0).getValue(), Item.EPSILON );
		
		for(int i=0; i<2; ++i) {
			assertEquals(coinTwentyPence.getValue(), insertedCoinBack.get(i+1).getValue(), Item.EPSILON );
		}
		
		for(int i=0; i<3; ++i) {
			assertEquals(coinFiftyPence.getValue(), insertedCoinBack.get(i+3).getValue(), Item.EPSILON );
		}
		
		for(int i=0; i<4; ++i) {
			assertEquals(coinOnePound.getValue(), insertedCoinBack.get(i+6).getValue(), Item.EPSILON );
		}
		

		assertEquals( 0, cashier.getInsertedCoinsBack().size() );
	}

	@Test
	public void testTotalInserted() {
		Wallet wallet = new Wallet(Wallet.DEFAULT_CURRENCY_UK);			
		wallet.addToPossible(coinTwentyPence);			
				
		Cashier cashier = new Cashier(wallet);

		
		for(int i=0; i<10; ++i) {
			cashier.insert(coinTwentyPence);
		}
		
		
		assertEquals(2.0f, cashier.totalInserted(), Item.EPSILON);
	}

	@Test
	public void testRemainedPayment() {
		Wallet wallet = new Wallet(Wallet.DEFAULT_CURRENCY_UK);			
		wallet.addToPossible(coinTwentyPence);			
				
		Cashier cashier = new Cashier(wallet);

		
		for(int i=0; i<10; ++i) {
			cashier.insert(coinTwentyPence);
		}
		
		Item item = new Item("ItemName", 3.5f);
		
		assertEquals(1.5f, cashier.remainedPayment(item), Item.EPSILON);
	}

	
	
	private static boolean calledOnNoEnoughtMoney = false;
	private static Item calledOnNoEnoughtMoneyItem = null;
	private static float calledOnNoEnoughtMoneyRemainedPayment = 0.f;
	
	private static boolean calledOnNoEnoughtMachineCoins = false;
	
	private static boolean calledOnTransactionCompleted	=false;
	private static Item calledOnTransactionCompletedItem = null;
	private static List<Coin> calledOnTransactionCompletedChangeCoins = null;

	private void resetPaymentsObserversTestState() {
		calledOnNoEnoughtMoney = false;
		calledOnNoEnoughtMoneyItem = null;
		calledOnNoEnoughtMoneyRemainedPayment = 0.f;
		
		calledOnNoEnoughtMachineCoins = false;
		
		calledOnTransactionCompleted	=false;
		calledOnTransactionCompletedItem = null;
		calledOnTransactionCompletedChangeCoins = null;
	}
	
	TransactionObserver transactionObserver = new TransactionObserver() {
		
		@Override
		public void onNoEnoughtMoney(Item item, float remainedPayment) {
			System.out.println("\nonNoEnoughtMoney");
			
			calledOnNoEnoughtMoney = true;
			calledOnNoEnoughtMoneyItem = item;
			calledOnNoEnoughtMoneyRemainedPayment = remainedPayment;
			
			System.out.println("No enought money to pay for Item : " + item.getName());	
			System.out.println("Remained payment : " + String.valueOf(remainedPayment) );	
		}
 
		@Override
		public void onNoEnoughtMachineCoins() {
			System.out.println("\nonNoEnoughtMachineCoins");
			
			calledOnNoEnoughtMachineCoins = true;
			
			System.out.println("Error: No enought machine coins to provide Change for selected Item");	
			System.out.println("========>End Transaction with Error");
			System.out.println("Select the other Item or click money's back button...");			
		}

		@Override
		public void onTransactionCompleted(Item item, List<Coin> changeCoins) {
			System.out.println("\nonTransactionCompleted");
			
			calledOnTransactionCompleted = true;
			calledOnTransactionCompletedItem = item;
			calledOnTransactionCompletedChangeCoins = changeCoins;
			
			System.out.println("Transaction is completed");
			
			System.out.println("Item : " + item.getName() + " is ready to collect by you.");
			System.out.println("Change provided : ");
			
			if(changeCoins.size() > 0 ) {
				System.out.println("\tCurrency : " + changeCoins.get(0).getCurrency());
				for(Coin coin : changeCoins) {
					System.out.println("\t\t" + String.valueOf(coin.getValue()) );
				}		
			}

			System.out.println("========>End Transaction with Success");
		}
	};
	
	
	private Cashier newCashierEnoughtForChange() {
		Wallet wallet = new Wallet(Wallet.DEFAULT_CURRENCY_UK);			
		wallet.addToPossible(coinTenPence);			
		wallet.addToPossible(coinTwentyPence);			
		wallet.addToPossible(coinFiftyPence);			
		wallet.addToPossible(coinOnePound);
				
		Cashier cashier = new Cashier(wallet);
		
		cashier.addObserver(transactionObserver);
		
		
		for(int i=0; i<2; ++i) {
			wallet.addCoin(TEN_PENCE);
		}
		
		for(int i=0; i<2; ++i) {
			wallet.addCoin(TWENTY_PENCE);
		}
		
		for(int i=0; i<2; ++i) {
			wallet.addCoin(FIFTY_PENCE);
		}
		
		for(int i=0; i<2; ++i) {
			wallet.addCoin(ONE_POUND);
		}
		
		return cashier;
	}
	
	@Test
	public void testPayForItem_EnoughtForChange() {

		Cashier cashier = newCashierEnoughtForChange();
		
		String ITEM_NAME = "ItemName";
		
		Item newItem = new Item(ITEM_NAME, 1.7f);

		{//calledOnNoEnoughtMoney
			resetPaymentsObserversTestState();
			List<Float> changeCoinsValues = cashier.payForItem(newItem);
			assertTrue(calledOnNoEnoughtMoney);
			assertEquals(1.7f, calledOnNoEnoughtMoneyRemainedPayment, Item.EPSILON);
			assertEquals(0, changeCoinsValues.size() );
		
		}
		
		{//transactionCompletedNoChange
			cashier.insert(coinFiftyPence);
			cashier.insert(coinOnePound);
			cashier.insert(coinTwentyPence);
			
			resetPaymentsObserversTestState();
			List<Float> changeCoinsValues = cashier.payForItem(newItem);
			assertEquals(0, changeCoinsValues.size() );
			
			assertTrue(calledOnTransactionCompleted);
			assertEquals(0, calledOnTransactionCompletedChangeCoins.size() );
			assertEquals(ITEM_NAME, calledOnTransactionCompletedItem.getName() );
			
		}
		 
		{//transactionCompleted Change
			
			System.out.println("totalInserted() : " + String.valueOf(cashier.totalInserted()  ) );
			
			cashier.insert(coinFiftyPence);
			cashier.insert(coinOnePound);
			cashier.insert(coinOnePound);
			resetPaymentsObserversTestState();
			List<Float> changeCoinsValues = cashier.payForItem(newItem);
			assertTrue(calledOnTransactionCompleted);
			assertEquals(ITEM_NAME, calledOnTransactionCompletedItem.getName() );
			
			assertEquals(changeCoinsValues.size(), calledOnTransactionCompletedChangeCoins.size());
			
			Float summationChangeCoinsValues =  changeCoinsValues
					.stream()
					.reduce(0.f, (a, b) -> a + b);
			
			Float summationCalledOnTransactionCompletedChangeCoins =  calledOnTransactionCompletedChangeCoins
					.stream()
					.map(Coin::getValue)
					.reduce(0.f, (a, b) -> a + b);
			
			assertEquals(summationChangeCoinsValues, summationCalledOnTransactionCompletedChangeCoins, Item.EPSILON);

			assertEquals(0.8f, summationChangeCoinsValues.floatValue(), Item.EPSILON);
					
			for(Float floatVal : changeCoinsValues) {
				System.out.println("CoinsValue :" + String.valueOf(floatVal) );
			}
			
			System.out.println( "ChangeCoinsValues" + String.valueOf( changeCoinsValues.size() ) );
		}
		
	}
	
	private Cashier newCashierEnoughtForChange_2() {
		Wallet wallet = new Wallet(Wallet.DEFAULT_CURRENCY_UK);			
		wallet.addToPossible(coinTenPence);			
		wallet.addToPossible(coinTwentyPence);			
		wallet.addToPossible(coinFiftyPence);			
		wallet.addToPossible(coinOnePound);					
				
		Cashier cashier = new Cashier(wallet);
		
		cashier.addObserver(transactionObserver);
		
		
		for(int i=0; i<20; ++i) {
			wallet.addCoin(TEN_PENCE);
		}
		
		
		return cashier;
	}
	
	@Test
	public void testPayForItem_EnoughtForChange_2() {

		Cashier cashier = newCashierEnoughtForChange_2();
		
		String ITEM_NAME = "ItemName";
		
		Item newItem = new Item(ITEM_NAME, 1.7f);
		
		{//transactionCompleted Change
			
			System.out.println("totalInserted() : " + String.valueOf(cashier.totalInserted()  ) );
			
			cashier.insert(coinFiftyPence);
			cashier.insert(coinOnePound);
			cashier.insert(coinOnePound);
			resetPaymentsObserversTestState();
			List<Float> changeCoinsValues = cashier.payForItem(newItem);
			assertTrue(calledOnTransactionCompleted);
			assertEquals(ITEM_NAME, calledOnTransactionCompletedItem.getName() );
			
			assertEquals(changeCoinsValues.size(), calledOnTransactionCompletedChangeCoins.size());
			
			Float summationChangeCoinsValues =  changeCoinsValues
					.stream()
					.reduce(0.f, (a, b) -> a + b);
			
			Float summationCalledOnTransactionCompletedChangeCoins =  calledOnTransactionCompletedChangeCoins
					.stream()
					.map(Coin::getValue)
					.reduce(0.f, (a, b) -> a + b);
			
			assertEquals(summationChangeCoinsValues, summationCalledOnTransactionCompletedChangeCoins, Item.EPSILON);

			assertEquals(0.8f, summationChangeCoinsValues.floatValue(), Item.EPSILON);
					
			for(Float floatVal : changeCoinsValues) {
				System.out.println("CoinsValue :" + String.valueOf(floatVal) );
			}
			
			System.out.println( "ChangeCoinsValues" + String.valueOf( changeCoinsValues.size() ) );
		}
		
	}
	
	private Cashier newCashierNoEnoughtForChange() {
		Wallet wallet = new Wallet(Wallet.DEFAULT_CURRENCY_UK);			
		wallet.addToPossible(coinTenPence);			
		wallet.addToPossible(coinTwentyPence);			
		wallet.addToPossible(coinFiftyPence);			
		wallet.addToPossible(coinOnePound);			
				
		Cashier cashier = new Cashier(wallet);
		
		cashier.addObserver(transactionObserver);
		
		
		for(int i=0; i<1; ++i) {
			wallet.addCoin(TEN_PENCE);
		}
		
		
		return cashier;
	}
	
	@Test
	public void testPayForItem_NoEnoughtForChange() {

		Cashier cashier = newCashierNoEnoughtForChange();
		
		String ITEM_NAME = "ItemName";
		
		Item newItem = new Item(ITEM_NAME, 1.7f);
		
		{//transactionCompleted Change
		
			cashier.insert(coinFiftyPence);
			cashier.insert(coinOnePound);
			cashier.insert(coinOnePound);
			resetPaymentsObserversTestState();
			
			assertEquals(2.5f, cashier.totalInserted(), Item.EPSILON);

			
			
			List<Float> changeCoinsValues = cashier.payForItem(newItem);
			assertTrue(calledOnNoEnoughtMachineCoins);
			
			assertEquals(0, changeCoinsValues.size());
			
			

			
			assertEquals(0, changeCoinsValues.size()); //after getInsertedCoinsBack this have to be "zero"

		}
		
	}
	
	
	@Test
	public void test_TotalInserted() {
		
		
		Cashier cashier = newCashierNoEnoughtForChange();
		cashier.insert(coinFiftyPence);
		cashier.insert(coinOnePound);
		cashier.insert(coinOnePound);
		
		Float summationTotalInsertedCoinsValues =  cashier.getInsertedCoinsBack()
				.stream()
				.map(Coin::getValue)
				.reduce(0.f, (a, b) -> a + b);
		
		assertEquals(2.5f, summationTotalInsertedCoinsValues.floatValue(), Item.EPSILON );
		
		
	}

}
