/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package items;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class ItemTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	private static final String ITEM_NAME = "ItemName";
	private static final float ITEM_PRICE = 0.1f;
	@Test
	public void testHashCode() {
		Item item = new Item(ITEM_NAME, ITEM_PRICE);
		assertEquals(ITEM_NAME.hashCode(), item.hashCode());
	}

	@Test
	public void testItemItem() {		
		Item item = new Item(ITEM_NAME, ITEM_PRICE);
		Item newItem = new Item(item);
		
		assertEquals(item.getName(), newItem.getName());
		assertEquals(item.getPrice(), newItem.getPrice(), Item.EPSILON);
	}
	
	@Test
	public void testItemsEquals() {		
		Item item = new Item(ITEM_NAME, ITEM_PRICE);
		
		Item newItem = new Item(ITEM_NAME, ITEM_PRICE);
		
		assertTrue( item.equals(newItem) );
	}

	@Test
	public void testGetName() {
		Item item = new Item(ITEM_NAME, ITEM_PRICE);
		assertEquals(ITEM_NAME, item.getName());
	}

	@Test
	public void testGetPrice() {
		Item item = new Item(ITEM_NAME, ITEM_PRICE);
		assertEquals(ITEM_PRICE, item.getPrice(), Item.EPSILON);
	}

	@Test
	public void testSetPrice() {
		Item item = new Item(ITEM_NAME, 0.f);
		item.setPrice(ITEM_PRICE);
		assertEquals(ITEM_PRICE, item.getPrice(), Item.EPSILON);
	}

}
