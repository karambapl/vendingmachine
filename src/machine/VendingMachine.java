/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package machine;

import java.util.List;
import items.Item;
import money.Cashier;
import money.Coin;
import money.CoinValidatorObserver;
import money.TransactionObserver;

public class VendingMachine implements  ItemsStorageObserver, TransactionObserver, CoinValidatorObserver{
	
	private Cashier cashier = null;
	private ItemsStorage itemsStorage = null;
	private boolean stateOn = false;
	
	
	public VendingMachine(Cashier cashier, ItemsStorage itemsStorage) {
		this.cashier = cashier;
		this.cashier.addObserver((TransactionObserver)this);
		this.cashier.addObserver((CoinValidatorObserver)this);
		
		this.itemsStorage = itemsStorage;
		this.itemsStorage.addObserver(this);
	}
	
	public boolean isOn() {
		return stateOn;
	}
	
	public void setOn() {
		stateOn = true;
	}
	
	public void setOff() {
		stateOn = false;
	}
	
	public boolean insertCoin(Coin coin) {
		if( isOn() ) {
			System.out.println("Coin inserted : " + coin.getValue() );
			cashier.insert(coin);
			
			return true;
		} else {
			System.out.println("The machine is off...");	
			return false;
		} 
	}
	
	public List<Coin> getMoneyBack() {
		return cashier.getInsertedCoinsBack();
	}
	
	public boolean selectItem(Item item) {
		if( isOn() ) {
			System.out.println("Item selected : " + item.getName() );
			itemsStorage.getItem(item.getName());
			
			return true;
		} else {
			System.out.println("The machine is off... Click money's back button...");	
			return false;
		} 
	}

	@Override
	public void onNoEnoughtMoney(Item item, float remainedPayment) {
		System.out.println("No enought money to pay for Item : " + item.getName());	
		System.out.println("Remained payment : " + String.valueOf(remainedPayment) );	
	}

	@Override
	public void onNoEnoughtMachineCoins() {
		System.out.println("Error: No enought machine coins to provide change for selected Item");	
		System.out.println("========>End Transaction with Error");
		System.out.println("Select the other Item or click money's back button...");			
	}

	@Override
	public void onTransactionCompleted(Item item, List<Coin> changeCoins) {
		
		System.out.println("Transaction is completed");
		
		System.out.println("Item : " + item.getName() + " is ready to collect by you.");
		System.out.println("change provided : ");
		System.out.println("\tCurrency : " + cashier.getWallet().getCurrency());
		for(Coin coin : changeCoins) {
			System.out.println("\t\t" + String.valueOf(coin.getValue()) );
		}		 

		System.out.println("========>End Transaction with Success");
	}

	@Override
	public void onItemAvailable(Item item) {
		System.out.println("onItemAvailable : " + item.getName());	
		System.out.println("========>BeginTransaction");
		cashier.payForItem(item);
	}

	@Override
	public void onNoItemInStorage(String itemName) {
		System.out.println("onNoItemInStorage : " + itemName);	
		System.out.println("Select the other Item or click money's back button...");	
	}

	@Override
	public void onCoinCurrencyInvalid(Coin coin, String validCurrency) {
		System.out.println("Error : onCoinCurrencyInvalid");
		System.out.println("ValidCurrency is : " + validCurrency);
		System.out.println("Returned user coin : ");
		System.out.println("\tCurrency : " + coin.getCurrency());
		System.out.println("\tValue : " + coin.getValue());
	}

	@Override
	public void onCoinValueInvalid(Coin coin, List<Float> validCoinsValues) {
		System.out.println("Error : onCoinValueInvalid");
		System.out.println("Valid values are : ");
		for(Float coinValue : validCoinsValues) {
			System.out.println("\t\t" + coinValue);
		}
		
		System.out.println("Returned user coin : ");
		System.out.println("\tCurrency : " + coin.getCurrency());
		System.out.println("\tValue : " + coin.getValue());
		
	}			
	

}
