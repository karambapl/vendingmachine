/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package machine;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import items.Item;

public class ItemsStorage {
	
	private Map<Integer, Integer> storageItems = new HashMap<>(); 
	private Map<Integer, Item> possibleItems = new HashMap<>();
	
	private Set<ItemsStorageObserver> observerSet = new HashSet<>();
	
	
	public ItemsStorage() {
		
	}
	
	public boolean addObserver(ItemsStorageObserver observer) {
		return observerSet.add(observer);
	}
	
	public boolean removeObserver(ItemsStorageObserver observer) {
		return observerSet.remove(observer);
	}

	public void addItem(Item item) { //infinity storage size :)
		int hashCode = item.hashCode();
		
		Integer itemCount = storageItems.get( hashCode );
		
		if ( itemCount != null ) {
			itemCount++;
			storageItems.put(hashCode, itemCount);
		} else {
			storageItems.put(hashCode, 1);
		}

		addToPossible(item);
	}

	private void addToPossible(Item item) {
		int hashCode = item.hashCode();//cached hash
		
		if( !possibleItems.containsKey(hashCode) ) {
			possibleItems.put(hashCode, item); 
		}		
		
	}
	
	public Map<Integer, Item> getPossibleItems() {
		return possibleItems;
	}	
	
	public Map<Integer, Integer> getStorageItemMap() {
		return storageItems;
	}
	
	public boolean isAvailable(String itemName) {
		int hashCode = itemName.hashCode();
		
		Integer itemCount = storageItems.get( hashCode );
		

		return UtilCheck.isIntegerValidNotZero(itemCount);
	}
 
	public Item getItem(String itemName) {
		 
		Item result = null;
		int hashCode = itemName.hashCode();
		
		Integer itemCount = storageItems.get( hashCode );
		
		if( UtilCheck.isIntegerValidNotZero(itemCount) ) {			
			
				result = new Item(possibleItems.get(hashCode));
				itemCount--;
				storageItems.put(hashCode, itemCount);
				
				callOnItemReadyToCollect(result);
			
		} else {
			callOnNoItemInStorage(itemName);
		}
		
		
		return result;
	}

	private void callOnNoItemInStorage(String itemName) {
		for(ItemsStorageObserver observer : observerSet) {
			observer.onNoItemInStorage(itemName);
		}			
	}

	private void callOnItemReadyToCollect(Item item) {
		for(ItemsStorageObserver observer : observerSet) {
			observer.onItemAvailable(item);
		}
	}	

}
