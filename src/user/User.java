/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package user;

import java.util.List;

import items.Item;
import machine.ItemsStorage;
import machine.VendingMachine;
import money.Cashier;
import money.Coin;
import money.Wallet;

public class User {

	private static final float ONE_POUND = 1.0f;
	private static final float FIFTY_PENCE = 0.5f;
	private static final float TWENTY_PENCE = 0.2f;
	private static final float TEN_PENCE = 0.1f;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Coin coinTenPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, TEN_PENCE);
		Coin coinTwentyPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, TWENTY_PENCE);
		Coin coinFiftyPence = new Coin(Wallet.DEFAULT_CURRENCY_UK, FIFTY_PENCE);
		Coin coinOnePound = new Coin(Wallet.DEFAULT_CURRENCY_UK, ONE_POUND);
		
		Item itemA = new Item("A", 0.6f);
		Item itemB = new Item("B", 1.0f);
		Item itemC = new Item("C", 1.7f);
		
		Wallet wallet = new Wallet(Wallet.DEFAULT_CURRENCY_UK);			
			wallet.addToPossible(coinTenPence);			
			wallet.addToPossible(coinTwentyPence);			
			wallet.addToPossible(coinFiftyPence);			
			wallet.addToPossible(coinOnePound);

		for (int i = 0; i < 10; ++i) {
			wallet.addCoin(TEN_PENCE);
			wallet.addCoin(TWENTY_PENCE);
			wallet.addCoin(FIFTY_PENCE);
			wallet.addCoin(ONE_POUND);
		}

		Cashier cashier = new Cashier(wallet);

		ItemsStorage itemsStorage = new ItemsStorage();
		
		for (int i = 0; i < 10; ++i) {			
			itemsStorage.addItem(itemA);
			itemsStorage.addItem(itemB);			
			itemsStorage.addItem(itemC);
		}

		VendingMachine vendingMachine = new VendingMachine(cashier, itemsStorage);
		vendingMachine.setOn();
		
			vendingMachine.selectItem(itemA);
			vendingMachine.insertCoin(coinTenPence);
			
			vendingMachine.selectItem(itemA);			
			vendingMachine.insertCoin(coinTwentyPence);
			
			vendingMachine.selectItem(itemA);
			vendingMachine.insertCoin(coinFiftyPence);
			
			vendingMachine.selectItem(itemA);
			vendingMachine.insertCoin(coinOnePound);
		
			List<Coin> myMoney = vendingMachine.getMoneyBack();
			
			System.out.println("\n\ngetMoneyBack : ");
			for(Coin coin : myMoney) {
				System.out.println("\tCurrency : " + coin.getCurrency());
				System.out.println("\t\t" + String.valueOf(coin.getValue()) );
			}		
		

	}

}
