/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package money;

import java.util.List;

import items.Item;

public interface TransactionObserver {
	
	void onNoEnoughtMoney(Item item, float remainedPayment);
	
	void onNoEnoughtMachineCoins();
	
	void onTransactionCompleted(Item item, List<Coin> changeCoins);
}
