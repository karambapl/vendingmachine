/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package money;

import java.util.List;

public interface CoinValidatorObserver {
	void onCoinCurrencyInvalid(Coin coin, String validCurrency);
	void onCoinValueInvalid(Coin coin, List<Float> validCoinsValues);
}
