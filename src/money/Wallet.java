/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package money;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import machine.UtilCheck;

public class Wallet {
	public static final String DEFAULT_CURRENCY_UK = "Pound";
	
	private Map<Float, Integer> walletsCoins = new HashMap<>();	 
	private List<Float> possibleCoins = new ArrayList<>();
	
	private String currency = "";
		
	public Wallet(String currency) {
		this.currency = currency;				
	}
	
	public void addCoin(float value) { //infinity wallet size :)
			
			Float valueF = new Float(value);
			
			Integer coinCount = walletsCoins.get(valueF);
			
			if ( coinCount != null ) {
				coinCount++;
				walletsCoins.put(valueF, coinCount);
			} else {
				walletsCoins.put(valueF, 1);
			}

	}
	
	public String getCurrency() {
		return currency;
	}
	
	public Map<Float, Integer> getWalletsCoinsMap() {
		return walletsCoins;
	}
	
	public List<Float> getPossibleCoins() {
		return possibleCoins;
	}
	  
	public boolean isValueValid(Float valueCheck) {
		return possibleCoins.contains( valueCheck );
	}		
	
	public boolean isCurrencyValid(String currencyCheck) {
		return currency.equalsIgnoreCase( currencyCheck );
	}		
	
	public boolean addToPossible(Coin coin) {
		if ( isCurrencyValid( coin.getCurrency() ) && !isValueValid( coin.getValue() ) ) {
			return possibleCoins.add( coin.getValue() );						
		} 
		
		return false; 
	}
	
	public float getCoin(float value) {
		
		float result = 0.f;
		
		Integer coinCount = walletsCoins.get(value);
		
		if( UtilCheck.isIntegerValidNotZero(coinCount) ) {			
				result = value;
				coinCount--;
				 
				walletsCoins.put(value, coinCount);
		}		
		
		return result;
	}

	
	public void addAll(List<Float> coinsValues) {
		for(Float coinValue : coinsValues) {
			addCoin(coinValue);
		}		
	}	
	
	
	public List<Float> getAll(List<Float> coinsToGet) {
		List<Float> result = new ArrayList<>();
		
		for(Float coinValue : coinsToGet) {
			
			float returnCoin = getCoin( coinValue );
			
			if( returnCoin > 0.f ) {
				result.add(returnCoin);
			}
		}	
		
		return result;
	}


}
