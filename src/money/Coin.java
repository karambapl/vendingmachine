/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package money;

public class Coin{
	public static int ACCURACY = 100;
	
	private String currency = null;
	private Float value = null;
	
	public Coin(String currency, Float value) {
		this.currency = currency;
		this.value = value;		
	}
	
	public Coin(Coin coin) {
		currency = new String(coin.getCurrency());
		value = new Float(coin.getValue());	
	}

	public String getCurrency() {
		return currency;
	}
	
	public Float getValue() {
		return value;
	}

}
