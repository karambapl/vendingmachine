/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package money;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import items.Item;

public class Cashier{
	
	private Comparator<Float> descendingOrder = new Comparator<Float>() {
		
		@Override
		public int compare(Float o1, Float o2) {			
			return Float.compare(o2, o1);
		}
	};
	
	private List<Float> insertedCoinsValues = new ArrayList<>();	
	private float totalInserted = 0.f;

	private Wallet wallet = null;
	private Set<TransactionObserver> transactionObserverSet = new HashSet<>();
	private Set<CoinValidatorObserver> coinValidatorObserverSet = new HashSet<>();
		
	public Cashier(Wallet wallet) {
		this.wallet = wallet;				
	}	
	
	public boolean addObserver(CoinValidatorObserver observer) {
		return coinValidatorObserverSet.add(observer);
	}
	
	public boolean removeObserver(CoinValidatorObserver observer) {
		return coinValidatorObserverSet.remove(observer);
	}
	
	public boolean addObserver(TransactionObserver observer) {
		return transactionObserverSet.add(observer);
	}
	
	public boolean removeObserver(TransactionObserver observer) {
		return transactionObserverSet.remove(observer);
	}
	
	public Wallet getWallet() {
		return wallet;
	}
	
	public boolean insert(Coin coin) {
		boolean result = validateCoin(coin);

		if( result ) {
			insertedCoinsValues.add(coin.getValue());	
			totalInserted += coin.getValue();
		}				
 
		return result;
	}
	
	private boolean validateCoin(Coin coin) {
		boolean result = false;
		
		if( wallet.isCurrencyValid(coin.getCurrency() ) ) {
			
			if( wallet.isValueValid(coin.getValue()) ) {
				result = true;
			} else {
				callOnCoinValueInvalid(coin);
			}			
			
		} else {
			callOnCoinCurrencyInvalid(coin);
		}
		
		return result;
	}

	private void resetInsertedCoinsValuesState() {
		insertedCoinsValues.clear();
		totalInserted = 0.f;
	}	
	
	public List<Coin> getInsertedCoinsBack() {
		List<Coin> moneyBack = generateCoinsFromValues(insertedCoinsValues)	;

		resetInsertedCoinsValuesState();
		return moneyBack;		
	}
	
	public float totalInserted() {
		return totalInserted;
	}

	public float remainedPayment(Item item) {		
		return item.getPrice() - totalInserted();
	}
	
	public List<Float> payForItem(Item item) {
		List<Float> changeCoinsValues = new ArrayList<>();
		
		float remainedPayment = remainedPayment(item);
		
		float remainedPaymentABS = Math.abs(remainedPayment);
		
		if( remainedPayment > Item.EPSILON) {
			onNoEnoughtMoney(item, remainedPayment);			
		} 
		else if( remainedPaymentABS < Item.EPSILON  ) {
			wallet.addAll(insertedCoinsValues);	
			resetInsertedCoinsValuesState();
			onTransactionCompleted(item, changeCoinsValues);			
		} 		
		else {
		
			wallet.addAll(insertedCoinsValues);
			changeCoinsValues = getChangeCoins( remainedPaymentABS );			
			
			resetInsertedCoinsValuesState();
			if( !changeCoinsValues.isEmpty() ) { 
				//so we are ready to 
				onTransactionCompleted(item, changeCoinsValues);
			}

		
		}
		
		return changeCoinsValues;

	}
	
	public List<Coin> generateCoinsFromValues(List<Float> coinValueList) {
		List<Coin> generated = new ArrayList<>();	
		
		for(Float coinValue : coinValueList) {
			generated.add(new Coin(wallet.getCurrency(), coinValue));
		}		 
		
		return generated;
	}
	
	private List<Float> getChangeCoins(float change)  {
		
		List<Float> changeCoinsValues = new ArrayList<>();	
		List<Float> walletCoinsValues = new ArrayList<>( wallet.getPossibleCoins() );
		
		Collections.sort(walletCoinsValues, descendingOrder);
		
		for( Float possibleCoinValue : walletCoinsValues) {
			
			int walletCoinValueACC = (int) (possibleCoinValue * Coin.ACCURACY);
			
			float changeValueACCFloat = (float) Math.ceil(Coin.ACCURACY * change);			
			int changeValueACC = (int) changeValueACCFloat;	
			
			int n = Math.floorDiv(changeValueACC, walletCoinValueACC);
			
			for( int i = 0; i < n; ++i ) {
				float coinValue = wallet.getCoin( possibleCoinValue );
				
				if (coinValue > Float.MIN_VALUE) {
					changeCoinsValues.add(coinValue);
					change -= possibleCoinValue;
					
				}
			}
			
		}			
		
		if( change > Item.EPSILON ) {
			wallet.addAll(changeCoinsValues);
			changeCoinsValues.clear();	
			
			//return inserted to wallet coins for wallet valid state
			wallet.getAll(insertedCoinsValues);
			
			callOnNoEnoughtMachineCoins();
		}			

		return changeCoinsValues;		
	}
	
	//================================================================================
	
	private void onNoEnoughtMoney(Item item, float remainedPayment) {		
		for(TransactionObserver observer : transactionObserverSet) {
			observer.onNoEnoughtMoney(item, remainedPayment);
		}
	}

	private void onTransactionCompleted(Item item, List<Float> changeValues) {		
		List<Coin> changeCoins = generateCoinsFromValues(changeValues);
		
		for(TransactionObserver observer : transactionObserverSet) {
			observer.onTransactionCompleted(item, changeCoins);
		}		
	}

	private void callOnNoEnoughtMachineCoins() {
		for(TransactionObserver observer : transactionObserverSet) {
			observer.onNoEnoughtMachineCoins();
		}		
	}
	
	//================================================================================
	 
	private void callOnCoinCurrencyInvalid(Coin coin) {		
		for(CoinValidatorObserver observer : coinValidatorObserverSet) {
			observer.onCoinCurrencyInvalid(coin, wallet.getCurrency());
		}		
	}
	
	private void callOnCoinValueInvalid(Coin coin) {		
		for(CoinValidatorObserver observer : coinValidatorObserverSet) {
			observer.onCoinValueInvalid(coin, wallet.getPossibleCoins());
		}		
	}

}
