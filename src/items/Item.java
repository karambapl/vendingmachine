/*
 * Copyright (c) 2017 Rafał Stańczuk <stanczuk.rafal@gmail.com>
 * 							https://www.linkedin.com/in/stanczuk/
 *							https://bitbucket.org/karambapl/
 *
 * This is proprietary software.
 * In no event shall the author be liable for any claim or damages.
 * All rights reserved. No warranty, explicit or implicit, provided.
 */

package items;

public class Item {
	public static final float EPSILON = 0.000001f;
	
	private String name;
	private float price;	
	
	public Item(Item item) {
		this.name = new String( item.getName() );
		this.price = item.getPrice();
	}
	
	public Item(String name, float price) {
		this.name = name;
		this.price = price;
	}	

	public String getName() {
		return name;
	}
	
	public float getPrice() {
		return price;
	}
	
	public void setPrice(float price) {
		this.price = price;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Item)) {
			return false;
		}
		
		Item item = (Item)obj;
		
		if( !(this.getName().equals(item.getName()) ) ) {
			return false;
		}
		
		if( Math.abs( this.getPrice() - item.getPrice() ) > EPSILON ) {
			return false;
		}		
		
		return true;
	}
	
	@Override
	public int hashCode() {		
		return name.hashCode();
	}

}
